package io.muic.ooc.stockmanager;

import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class MyAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        Iterable<User> userIterable = userRepository.findAll();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        for (User user : userIterable) {
            if (name.equals(user.getUsername()) && encoder.matches(password, user.getPassword())){

                // use the credentials
                // and authenticate against the third-party system

                return new UsernamePasswordAuthenticationToken(
                        name, password, new ArrayList<>());
            }
        }
        return null;

    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}