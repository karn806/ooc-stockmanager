package io.muic.ooc.stockmanager.configuration;

import io.muic.ooc.stockmanager.entity.Privilege;
import io.muic.ooc.stockmanager.entity.Role;
import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.PrivilegeRepository;
import io.muic.ooc.stockmanager.repository.RoleRepository;
import io.muic.ooc.stockmanager.repository.UserRepository;
import io.muic.ooc.stockmanager.service.AddUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * Created by May on 4/5/2017 AD.
 */
@Component
public class InitialDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;


    @Autowired
    private AddUserService addUserService;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (alreadySetup)
            return;

        Privilege decreaseItemPrivilege
                = createPrivilegeIfNotFound("DECREASE_ITEM_PRIVILEGE");
        Privilege editItemPrivilege
                = createPrivilegeIfNotFound("EDIT_ITEM_PRIVILEGE");
        Privilege increaseItemPrivilege
                = createPrivilegeIfNotFound("INCREASE_ITEM_PRIVILEGE");
        Privilege userPrivilege
                = createPrivilegeIfNotFound("USER_PRIVILEGE");
        Privilege addItemPrivilege
                = createPrivilegeIfNotFound("ADD_ITEM_PRIVILEGE");


        List<Privilege> adminPrivileges = Arrays.asList(decreaseItemPrivilege,editItemPrivilege,increaseItemPrivilege,userPrivilege,addItemPrivilege);
        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_STAFF", Arrays.asList(editItemPrivilege,increaseItemPrivilege,decreaseItemPrivilege,addItemPrivilege));
        createRoleIfNotFound("ROLE_CASHIER", Arrays.asList(decreaseItemPrivilege));
        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
        User admin = userRepository.findByUsername("testAdmin");
        if(admin==null){
            addUserService.addUser(Arrays.asList("testAdmin","test","admin","test","admin@muic.com","0819999999","no have","admin"));
        }

    }

    @Transactional
    private Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    private Role createRoleIfNotFound(
            String name, List<Privilege> privileges) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
