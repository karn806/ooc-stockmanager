package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.entity.Privilege;
import io.muic.ooc.stockmanager.entity.Tag;
import io.muic.ooc.stockmanager.entity.TagType;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import io.muic.ooc.stockmanager.repository.TagTypeRepository;
import io.muic.ooc.stockmanager.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class AddItemController {

    @Autowired
    private AddItemService addItemService;



    @Autowired
    private TagTypeService tagTypeService;

    @Autowired
    TagTypeRepository tagTypeRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;

    final String privilege = "ADD_ITEM_PRIVILEGE";

    @GetMapping(value = {"/addItem"})
    public String generateView(Model model) {
        if(authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "addItem";
        }
        return "redirect:/accessDenied";

    }

    @PostMapping("/addItem")
    public String addItemSubmit(Model model, @RequestParam(value = "brand") String brand,
                                @RequestParam(value = "price") String price,
                                @RequestParam(value = "tags") List<String> tags,
                                @RequestParam(value = "type") String type){
        if(!authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "redirect:/accessDenied";
        }
        String name = type + " " + brand;
        for (String tag:tags) {
            System.out.println(tag);
            name += " " + tag;
        }

        List<Item> item = itemRepository.findByName(name);
        if(item.isEmpty()){
            Item newItem = new Item();
            List<Tag> allTags = newItem.getTags();
            Iterable<TagType> tagTypeIterable = tagTypeRepository.findByTypeName(type);
            for (String tag:tags) {
                System.out.println(tag);
                Tag each_tag = new Tag();
                for (TagType tagDB:tagTypeIterable) {
                    each_tag.setTagType(tagDB);
                }
                each_tag.setValue(tag);
                allTags.add(each_tag);
            }
            newItem.setName(name);
            newItem.setBrand(brand);
            newItem.setPrice(Double.parseDouble(price));
            newItem.setTags(allTags);
            newItem.setTypeName(type);
            newItem.setStatus("active");
            addItemService.addItemToDB(newItem);
            System.out.println("Add item to database successfully");
        } else {
            if(item.get(0).getPrice() == Double.parseDouble(price)
                    && item.get(0).getName().equals(name)
                    && item.get(0).getStatus().equals("inactive")){
                item.get(0).setStatus("active");
                addItemService.addItemToDB(item.get(0));
                System.out.println("here");
            } else {
                System.out.println("item already exist!");
                model.addAttribute("errorMsg", "Item already exist.");
                return generateView(model);
            }

        }
        return "redirect:/dashboard";

    }

}
