package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.TagType;
import io.muic.ooc.stockmanager.repository.TagTypeRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.TagTypeService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class AddTagTypeController {

    @Autowired
    TagTypeService tagTypeService;

    @Autowired
    TagTypeRepository tagTypeRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;


    final String privilege = "EDIT_ITEM_PRIVILEGE";

    @GetMapping(value = {"/addTagType"})
    public String generateView(Model model) {
        if(authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "addTagType";
        }
        return "redirect:/accessDenied";
    }

    @PostMapping(value = {"/addTagType"})
    public String addTagType(Model model, @RequestParam(value="type_name") String type_name ,
                             @RequestParam(value="possibleValues") List<String> tags){
//        for (String t:tags) {
//            System.out.println(t);
//        }
        if(!authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "redirect:/accessDenied";
        }
        List<TagType> tagTypes = tagTypeRepository.findByTypeName(type_name);
        if(tagTypes.isEmpty()){
            TagType tagType = new TagType();
            tagType.setTypeName(type_name);
            tagType.setPossibleValues(tags);
            tagTypeService.addTagType(tagType);
            System.out.println("Add tag type to database successfully");
        } else {
            model.addAttribute("errorMsg", "This tag type already exist.");
            return generateView(model);
        }

        return "redirect:/manageTagType";
    }

}

