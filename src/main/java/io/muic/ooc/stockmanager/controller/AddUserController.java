package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.Role;
import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.RoleRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import io.muic.ooc.stockmanager.service.AddUserService;

import java.lang.reflect.Array;
import java.util.Arrays;

@Controller
public class AddUserController {

    @Autowired
    private AddUserService addUserService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;

    final String privilege = "USER_PRIVILEGE";

    @GetMapping(value = {"/addUser"})
    public String addUserPage(Model model) {
        if(authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "addUser";
        }
        return "redirect:/accessDenied";
    }

    @PostMapping("/addUser")
    public String addUserSubmit(@RequestParam("username") String username,
                                @RequestParam("password") String password,
                                @RequestParam("firstName") String firstName,
                                @RequestParam("lastName") String lastName,
                                @RequestParam("email") String email,
                                @RequestParam("phone") String phone,
                                @RequestParam("address") String address,
                                @RequestParam("role") String role) {
        if(!authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "redirect:/accessDenied";
        }
        addUserService.addUser(Arrays.asList(username,password,firstName,lastName,email,phone,address,role));
        System.out.println("Add user to database successfully");
        return "redirect:/userList";
    }
}
