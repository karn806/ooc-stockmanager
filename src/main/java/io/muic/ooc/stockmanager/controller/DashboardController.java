package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import io.muic.ooc.stockmanager.repository.UserRepository;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller

public class DashboardController {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private UserService userService;


    @GetMapping(value = {"/dashboard"})
    public String index(Model model) {
        Iterable<Item> itemIterable = itemRepository.findAll();

        List<Item> items = new ArrayList<>();

        for (Item item:itemIterable) {
            items.add(item);
        }

        model.addAttribute("items", items);
        model.addAttribute("user", userService.getPrincipal());
        model.addAttribute("role",userService.myRole());
        return "dashboard";
    }
}
