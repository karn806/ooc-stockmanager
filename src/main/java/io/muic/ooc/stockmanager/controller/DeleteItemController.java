package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class DeleteItemController {


    @Autowired
    ItemRepository itemRepository;


    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;

    final String privilege = "EDIT_ITEM_PRIVILEGE";

    @PostMapping("/deleteItem")

    public String deleteItem(@RequestParam(value = "selectedItem") String itemName){
        if(!authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "redirect:/accessDenied";
        }
        System.out.println("Enter post in deleteItem controller");
        System.out.println("selectedItem =" + itemName);
        List<Item> items = itemRepository.findByName(itemName);
        Item item = items.get(0);
        System.out.println((item!= null));

        if (item != null){
            item.setStatus("inactive");
            System.out.println("Item exist");
            itemRepository.save(item);

        }else {
            System.out.println("Item does not exist");
        }
        return "redirect:/dashboard";

    }
}
