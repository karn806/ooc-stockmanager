package io.muic.ooc.stockmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.ItemService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class EditItemController {

    @Autowired
    ItemService itemService;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;

    final String privilege = "EDIT_ITEM_PRIVILEGE";

    @GetMapping(value = {"/editItem"})
    public String generateView(Model model, @RequestParam(value = "selectedItem") String itemName) {
        if(authorizationService.autorized(userService.getPrincipal(),privilege)){
            List<Item> items = itemRepository.findByName(itemName);
            Item item = items.get(0);
            model.addAttribute("itemName", itemName);
            model.addAttribute("itemPrice", item.getPrice());
            return "editItem";
        }
        return "redirect:/accessDenied";
    }

    @PostMapping
    public String editItem(@RequestParam(value = "price") String price,
                           @RequestParam(value = "itemName") String itemName){
        if(!authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "redirect:/accessDenied";
        }
        List<Item> items = itemRepository.findByName(itemName);
        Item item = items.get(0);
        item.setPrice(Double.parseDouble(price));
        itemService.updateToDB(item);
        return "redirect:/dashboard";
    }
}
