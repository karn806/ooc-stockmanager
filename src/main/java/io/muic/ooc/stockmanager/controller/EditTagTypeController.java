package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.TagType;
import io.muic.ooc.stockmanager.repository.TagTypeRepository;
import io.muic.ooc.stockmanager.service.TagTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class EditTagTypeController {

    @Autowired
    TagTypeRepository tagTypeRepository;

    @GetMapping(value = {"/editTagType"})
    public String generateView(Model model, @RequestParam(value = "selectedTagType") String selectedTagType) {
        List<TagType> tagTypes = tagTypeRepository.findByTypeName(selectedTagType);
        model.addAttribute("typeName", selectedTagType);
        model.addAttribute("posVals", tagTypes.get(0).getPossibleValues());
        return "editTagType";
    }

    @PostMapping(value = {"/editTagType"})
    public String editTagType(Model model, @RequestParam(value = "possibleValues") List<String> posVal,
                              @RequestParam(value = "type_name") String typeName) {
        List<TagType> tagTypes = tagTypeRepository.findByTypeName(typeName);
        TagType tagType = tagTypes.get(0);
        tagType.setPossibleValues(posVal);
        tagTypeRepository.save(tagType);
        return "redirect:/manageTagType";
    }
}
