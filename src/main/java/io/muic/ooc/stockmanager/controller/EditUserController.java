package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.UserRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.EditUserService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

@Controller
public class EditUserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    EditUserService editUserService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;

    final String privilege = "USER_PRIVILEGE";

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public String editUser(Model model, @RequestParam("username") String username) {
        if(authorizationService.autorized(userService.getPrincipal(),privilege)){
            model.addAttribute("username", username);
            return "editUser";
        }
        return "redirect:/accessDenied";
    }
    @RequestMapping(value = "/editUser", method = RequestMethod.POST)
    public String editUserSubmit(@RequestParam("username") String username,
                                 @RequestParam("password") String password,
                                 @RequestParam("firstName") String firstName,
                                 @RequestParam("lastName") String lastName,
                                 @RequestParam("email") String email,
                                 @RequestParam("phone") String phone,
                                 @RequestParam("address") String address){
        if(!authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "redirect:/accessDenied";
        }
        editUserService.editUser(Arrays.asList(username,password,firstName,lastName,email,phone,address));
        return "redirect:/userList";

    }
}
