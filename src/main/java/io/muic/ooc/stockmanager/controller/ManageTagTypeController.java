package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.TagType;
import io.muic.ooc.stockmanager.repository.TagTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;


@Controller
public class ManageTagTypeController {

    @Autowired
    TagTypeRepository tagTypeRepository;

    @GetMapping(value = {"/manageTagType"})
    public String index(Model model) {

        Iterable<TagType> tagTypeIterable = tagTypeRepository.findAll();

        List<TagType> tagTypes = new ArrayList<>();

        for (TagType tagType : tagTypeIterable) {
            tagTypes.add(tagType);
        }

        model.addAttribute("tagTypes", tagTypes);

        return "tagType";
    }
}
