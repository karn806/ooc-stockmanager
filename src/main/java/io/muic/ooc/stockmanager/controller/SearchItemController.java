package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by arparnuch on 3/28/2017 AD.
 */
@Controller
public class SearchItemController {
    @Autowired
    ItemRepository itemRepository;

    @RequestMapping(value = "/searchItem", method = RequestMethod.GET)
    public String searchUser(Model model){
        return "searchItem";
    }
    @RequestMapping(value = "/searchItem", method = RequestMethod.POST)
    public String searchItemSubmit(Model model, @RequestParam("searchInput") String searchInput){
        List<Item> items = itemRepository.findByName(searchInput);
        model.addAttribute("items", items);
        return "searchItem";
    }
}
