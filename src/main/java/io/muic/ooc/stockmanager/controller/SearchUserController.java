package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class SearchUserController {
    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/searchUser", method = RequestMethod.GET)
    public String searchUser(Model model){
        return "searchUser";
    }
    @RequestMapping(value = "/searchUser", method = RequestMethod.POST)
    public String searchUserSubmit(Model model, @RequestParam("searchInput") String searchInput){
        List<User> users = userRepository.findByFirstName(searchInput);
        model.addAttribute("users", users);
        return "searchUser";
    }
}
