package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.UserRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by May on 4/6/2017 AD.
 */
@Controller
public class SecurityController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", userService.getPrincipal());
        return "accessDenied";
    }
    @GetMapping(value = {"/", "/login"})
    public String login(Model model) {
        if(userService.isLogin()){
            return "redirect:/dashboard";
        }
        return "login";
    }




}
