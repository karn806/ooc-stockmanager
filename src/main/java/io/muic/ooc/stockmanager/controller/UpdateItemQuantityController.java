package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.ItemService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;


@Controller
public class UpdateItemQuantityController {
    @Autowired
    ItemRepository itemRepository;

    @Autowired
    ItemService itemService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;

    final String privilege = "ADD_ITEM_PRIVILEGE";

    @GetMapping(value = {"/updateQuantity"})
    public String generateView(Model model, @RequestParam(value = "selectedItem") String itemName) {
        List<Item> items = itemRepository.findByName(itemName);
        Item item = items.get(0);
        double price = item.getPrice();
        int quantity = item.getQuantity();
        model.addAttribute("itemName", itemName);
        model.addAttribute("quantity", quantity);
        model.addAttribute("price", price);
        return "updateQuantity";
    }


    @PostMapping("/updateQuantity")
    public String updateItemQuantity(@RequestParam(value = "price") double price,
                                     @RequestParam(value = "itemName") String itemName,
                                     @RequestParam(value = "quantity") int quantity) {

        List<Item> items = itemRepository.findByName(itemName);
        Item item = items.get(0);
        item.setQuantity(quantity);
        itemService.updateToDB(item);
        return "redirect:/dashboard";
    }
}
