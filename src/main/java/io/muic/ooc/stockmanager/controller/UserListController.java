package io.muic.ooc.stockmanager.controller;

import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.UserRepository;
import io.muic.ooc.stockmanager.service.AuthorizationService;
import io.muic.ooc.stockmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UserListController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationService authorizationService;

    final String privilege = "USER_PRIVILEGE";

    @RequestMapping(value = "/userList", method = RequestMethod.GET)
    public String userList(Model model) {
        if(authorizationService.autorized(userService.getPrincipal(),privilege)){
            Iterable<User> userIterable = userRepository.findAll();

            String currentUser = userService.getPrincipal().getUsername();
            List<User> users = new ArrayList<>();

            for (User user: userIterable) {
                if (!currentUser.equals(user.getUsername())) {
                    users.add(user);
                }
            }

            model.addAttribute("users", users);
            return "userList";
        }
        return "redirect:/accessDenied";

    }
    @RequestMapping(value = "/userList", method = RequestMethod.POST)
    public String userListSubmit(Model model,
                                 @RequestParam("username") String username,
                                 @RequestParam("action") String action){
//        System.out.println("delete: "+action);
        if(!authorizationService.autorized(userService.getPrincipal(),privilege)){
            return "redirect:/accessDenied";
        }
        if (action.equals("delete")){
            Iterable<User> userIterable = userRepository.findAll();
            for (User user : userIterable) {
                if (user.getUsername().equals(username)) {
                    userRepository.delete(user);
                }
            }
        }
        else if (action.equals("edit")){
            model.addAttribute("username", username);
            return "redirect:/editUser";
        }

        return "redirect:/userList";
    }

}
