package io.muic.ooc.stockmanager.controller.rest_controller;

import io.muic.ooc.stockmanager.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class ItemTagsController {

    @Autowired
    ItemService itemService;

//    @GetMapping(value = {"/itemTags"})
//    public List<Map<String,List<String>>> searchItemInfo(@RequestParam(value = "itemName") String itemName){
//
//        List<Map<String,List<String>>> items = itemService.searchItemTag(itemName);
//        return items;
//    }

    @GetMapping(value = {"/itemTags"})
    public Map<String, List<Map<String,String>>> searchItemInfo(@RequestParam(value = "itemName") String itemName){

        Map<String, List<Map<String,String>>> items = itemService.searchItemTag(itemName);
        return items;
    }
}
