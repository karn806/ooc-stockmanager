package io.muic.ooc.stockmanager.controller.rest_controller;

import io.muic.ooc.stockmanager.repository.TagTypeRepository;
import io.muic.ooc.stockmanager.service.TagTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class PosValTagTypeController {
    @Autowired
    TagTypeService tagTypeService;

    @GetMapping(value = {"/posValTagType"})
    public Map< String, List< Map<String, String> >> searchPossibleValues(@RequestParam(value="type") String type){
        Map< String, List< Map<String, String> > > posVal = tagTypeService.searchPosVal(type);
        return posVal;
    }
}
