package io.muic.ooc.stockmanager.controller.rest_controller;

import io.muic.ooc.stockmanager.service.TagTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class PossibleValuesController {

    @Autowired
    TagTypeService tagTypeService;

//    @CrossOrigin(origins = "http://139.59.101.18:8080")
    @GetMapping(value = {"/possibleValues"})
    public Map searchPossibleValues(@RequestParam(value="type") String type){
        Map posVal = tagTypeService.searchPosVal(type);
        return posVal;
    }
}
