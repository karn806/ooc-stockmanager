package io.muic.ooc.stockmanager.controller.rest_controller;

import io.muic.ooc.stockmanager.entity.TagType;
import io.muic.ooc.stockmanager.service.TagTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class TagTypeController {

    @Autowired
    TagTypeService tagTypeService;

//    @CrossOrigin(origins = "http://139.59.101.18:8080")
    @GetMapping(value = {"/tagType"})
    public Map searchTagType() {
        Map tagTypeNames = tagTypeService.searchAllTag();
        return tagTypeNames;
    }
}
