package io.muic.ooc.stockmanager.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TagType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String typeName;

    @ElementCollection
    @CollectionTable(name = "join_pos_values", joinColumns = @JoinColumn(name = "tag_type_id"))
    @Column(name = "value")
    private List<String> possibleValues;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<String> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<String> possibleValues) {
        this.possibleValues = possibleValues;
    }
}
