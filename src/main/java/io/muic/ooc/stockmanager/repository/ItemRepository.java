package io.muic.ooc.stockmanager.repository;

import io.muic.ooc.stockmanager.entity.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ItemRepository extends CrudRepository<Item, Long>{
    List<Item> findByName(String name);
}
