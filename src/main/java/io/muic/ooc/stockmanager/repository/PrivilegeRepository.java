package io.muic.ooc.stockmanager.repository;

import io.muic.ooc.stockmanager.entity.Privilege;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by May on 4/5/2017 AD.
 */
public interface PrivilegeRepository  extends CrudRepository<Privilege, Long> {
    Privilege findByName(String name);
}
