package io.muic.ooc.stockmanager.repository;

import io.muic.ooc.stockmanager.entity.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by May on 4/5/2017 AD.
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByName(String name);
}
