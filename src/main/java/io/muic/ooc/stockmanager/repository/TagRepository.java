package io.muic.ooc.stockmanager.repository;

import io.muic.ooc.stockmanager.entity.Tag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long> {

}
