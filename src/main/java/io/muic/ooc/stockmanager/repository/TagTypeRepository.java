package io.muic.ooc.stockmanager.repository;

import io.muic.ooc.stockmanager.entity.TagType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagTypeRepository extends CrudRepository<TagType, Long> {
    List<TagType> findByTypeName(String typeName);

}
