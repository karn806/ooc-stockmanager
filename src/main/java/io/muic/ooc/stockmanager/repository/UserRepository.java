package io.muic.ooc.stockmanager.repository;

import io.muic.ooc.stockmanager.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
    List<User> findByFirstName(String firstname);
}
