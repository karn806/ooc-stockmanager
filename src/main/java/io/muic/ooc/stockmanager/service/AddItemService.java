package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddItemService {

    @Autowired
    private ItemRepository itemRepository;

    public void addItemToDB(Item item) {
        itemRepository.save(item);
    }

}
