package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.Tag;
import io.muic.ooc.stockmanager.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddTagService {
    @Autowired
    TagRepository tagRepository;

    public void addTagToDB(Tag tag) {
        tagRepository.save(tag);
    }
}
