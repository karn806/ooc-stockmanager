package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.Role;
import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.RoleRepository;
import io.muic.ooc.stockmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@Service
public class AddUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public void addUser(List<String> userParams){
        User user = new User();
        user.setUsername(userParams.get(0));
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashPass = encoder.encode(userParams.get(1));
        user.setPassword(hashPass);
        user.setFirstName(userParams.get(2));
        user.setLastName(userParams.get(3));
        user.setEmail(userParams.get(4));
        user.setPhone(userParams.get(5));
        user.setAddress(userParams.get(6));

        String role = userParams.get(7);
        Role userRole;
        if(role.equals("admin")){
            userRole = roleRepository.findByName("ROLE_ADMIN");
        }
        else if(role.equals("staff")){
            userRole = roleRepository.findByName("ROLE_STAFF");
        }
        else {
            userRole = roleRepository.findByName("ROLE_CASHIER");
        }
        user.setRoles(Arrays.asList(userRole));
        userRepository.save(user);
    }
}
