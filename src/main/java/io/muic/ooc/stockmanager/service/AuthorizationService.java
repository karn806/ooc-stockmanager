package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.Privilege;
import io.muic.ooc.stockmanager.entity.Role;
import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.RoleRepository;
import io.muic.ooc.stockmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by May on 4/6/2017 AD.
 */
@Service
public class AuthorizationService {
    @Autowired
    private RoleRepository roleRepository;

    public boolean autorized(User user, String privilege){
        for (Role role:user.getRoles()) {
            for (Privilege privilege1:role.getPrivileges()) {
                if(privilege.equals(privilege1.getName())){
                    return true;
                }
            }
        }
        return false;
    }
}
