package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.Role;
import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.RoleRepository;
import io.muic.ooc.stockmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class EditUserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;


    public void editUser(List<String> userParams){
        User user = userRepository.findByUsername(userParams.get(0));
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!userParams.get(1).equals("")){
            String hashPass = encoder.encode(userParams.get(1));
            user.setPassword(hashPass);
        }
        if (!userParams.get(2).equals("")){
            user.setFirstName(userParams.get(2));
        }
        if (!userParams.get(3).equals("")){
            user.setLastName(userParams.get(3));
        }
        if (!userParams.get(4).equals("")){
            user.setEmail(userParams.get(4));
        }
        if (!userParams.get(5).equals("")){
            user.setPhone(userParams.get(5));
        }
        if (!userParams.get(6).equals("")){
            user.setAddress(userParams.get(6));
        }
//        if (!userParams.get(7).equals("")){
//            String role = userParams.get(7);
//            Role userRole;
//            if(role.equals("admin")){
//                userRole = roleRepository.findByName("ROLE_ADMIN");
//            }
//            else if(role.equals("staff")){
//                userRole = roleRepository.findByName("ROLE_STAFF");
//            }
//            else {
//                userRole = roleRepository.findByName("ROLE_CASHIER");
//            }
//            user.setRoles(Arrays.asList(userRole));
//        }

        userRepository.save(user);
    }
}
