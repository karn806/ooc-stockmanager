package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.Item;
import io.muic.ooc.stockmanager.entity.Tag;
import io.muic.ooc.stockmanager.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

//    public List<Map<String, String>> searchItemInfo(String itemName) {
//        List< Map<String, String> > response = new ArrayList<>();
//        Iterable<Item> itemIterable = itemRepository.findByName(itemName);
//        for (Item each_item:itemIterable) {
//            Map<String, String> item = new HashMap<>();
//            item.put("Id", Long.toString(each_item.getId()));
//            item.put("Brand", each_item.getBrand());
//            item.put("Price", Double.toString(each_item.getPrice()));
//            item.put("TypeName", each_item.getTypeName());
//            response.add(item);
//        }
//        return response;
//    }

    public Map<String, List<Map<String, String>>> searchItemInfo(String itemName) {
        Map<String,List<Map<String, String>>> response = new HashMap<>();
        Iterable<Item> itemIterable = itemRepository.findByName(itemName);
        List<Map<String, String>> items = new ArrayList<>();
        for (Item each_item:itemIterable) {
            Map<String, String> item = new HashMap<>();
            item.put("Id", Long.toString(each_item.getId()));
            item.put("Brand", each_item.getBrand());
            item.put("Price", Double.toString(each_item.getPrice()));
            item.put("TypeName", each_item.getTypeName());
            items.add(item);
        }
        response.put("items", items);
        return response;
    }

//    public List<Map<String,List<String>>> searchItemTag(String itemName){
//        List<Map<String,List<String>>> response = new ArrayList<>();
//        Iterable<Item> itemIterable = itemRepository.findByName(itemName);
//
//        for (Item each_item:itemIterable) {
//            Map<String, List<String>> tags = new HashMap<>();
//            List<String> temp = new ArrayList<>();
//            for (Tag tag: each_item.getTags()){
//                temp.add(tag.getValue());
//            }
//            tags.put("Tags", temp);
//            response.add(tags);
//        }
//
//        return response;
//    }

    public Map<String, List<Map<String,String>>> searchItemTag(String itemName){
        Map<String, List<Map<String,String>>> response = new HashMap<>();
        Iterable<Item> itemIterable = itemRepository.findByName(itemName);
        List<Map<String, String>> tags = new ArrayList<>();
        for (Item each_item:itemIterable) {

            for (Tag tag: each_item.getTags()){
                Map<String, String> val = new HashMap<>();
                val.put("val", tag.getValue());
                tags.add(val);
            }

            response.put("Tags", tags);
        }

        return response;
    }

    public void updateToDB(Item item) {
        itemRepository.save(item);
    }
}
