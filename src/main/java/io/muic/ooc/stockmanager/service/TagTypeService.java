package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.TagType;
import io.muic.ooc.stockmanager.repository.TagTypeRepository;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TagTypeService {

    @Autowired
    TagTypeRepository tagTypeRepository;

    public void addTagType(TagType tagType) {
        tagTypeRepository.save(tagType);
    }

//    public List<TagType> searchAllTag(){
//        List<TagType> tagTypes = new ArrayList<>();
//        Map<String, List<TagType>> response = new HashMap<>();
//        Iterable<TagType> tagTypeIterable = tagTypeRepository.findAll();
//        for (TagType tag:tagTypeIterable) {
//            tagTypes.add(tag);
//        }
//
////        response.put("tagType", MapUtils.putAll(new HashMap<>(),tagTypes ));
//        return tagTypes;
//    }

    public Map searchAllTag(){
        Map< String, List< Map<String, String> > > response = new HashMap<>();
        Iterable<TagType> tagTypeIterable = tagTypeRepository.findAll();
        List< Map<String, String> > temp = new ArrayList<>();
        int count = 1;
        for (TagType tag:tagTypeIterable) {
            Map<String, String> tagTypes = new HashMap<>();
//            tagTypes.put("id", Integer.toString(count));
            tagTypes.put("id", tag.getId().toString());
            tagTypes.put("tagTypeName",tag.getTypeName());
            if(response.get("tagTypeName") != null){
                temp = response.get("tagTypeName");
            }
            temp.add(tagTypes);
            count += 1;
        }
        response.put("tagType", temp);
        return response;
    }

    public Map< String, List< Map<String, String> > > searchPosVal(String type) {
        Map< String, List< Map<String, String> > > response = new HashMap<>();
        Iterable<TagType> tagTypeIterable =  tagTypeRepository.findByTypeName(type);
        List< Map<String, String> > temp = new ArrayList<>();
        for (TagType tag:tagTypeIterable) {
            List<String> posVals = tag.getPossibleValues();
            int count = 1;
            for (String val: posVals) {
                Map<String, String> posVal = new HashMap<>();
                posVal.put("id", Integer.toString(count));
                posVal.put("val", val);
                temp.add(posVal);
                count += 1;
            }
        }
        response.put("PossibleValues", temp);
        return response;
    }

//    public Map searchPosValTagType(String tagTypeName) {
//        Map<String, List<Map<String, String>>> response = new HashMap<>();
//        Iterable<TagType> tagTypeIterable =  tagTypeRepository.findByTypeName(tagTypeName);
//        List< Map<String, String> > temp = new ArrayList<>();
//        for (TagType tag:tagTypeIterable) {
//            List<String> posVals = tag.getPossibleValues();
//            int count = 1;
//            for (String val: posVals) {
//                Map<String, String> posVal = new HashMap<>();
//                posVal.put("id", Integer.toString(count));
//                posVal.put("val", val);
//                temp.add(posVal);
//                count += 1;
//            }
//        }
//        response.put("PossibleValues", temp);
//        return response;
//    }
}
