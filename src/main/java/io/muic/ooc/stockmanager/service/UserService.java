package io.muic.ooc.stockmanager.service;

import io.muic.ooc.stockmanager.entity.User;
import io.muic.ooc.stockmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Created by May on 4/6/2017 AD.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
//        System.out.println(userName);
        User user = userRepository.findByUsername(userName);
//        System.out.println(user.getUsername());
        return user;
    }

    public boolean isLogin(){
        return getPrincipal()!=null;
    }

    public String myRole(){
        return getPrincipal().getRoles().get(0).getName();
    }
}
