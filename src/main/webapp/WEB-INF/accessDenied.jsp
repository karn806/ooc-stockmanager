
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>AccessDenied page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .wrapper{
            width: 70%;
            margin: 10% auto 0 auto;
        }

    </style>
</head>
<body>
    <div class="container">
        <div class="wrapper">
            <div class="jumbotron">
                <p>Dear <strong>${user.getFirstName()}</strong>, You are not authorized</p>
                </ br>
                <a href="/dashboard"><button class="btn btn-warning">To Dashboard</button></a>
            </div>
        </div>
    </div>
</body>
</html>
