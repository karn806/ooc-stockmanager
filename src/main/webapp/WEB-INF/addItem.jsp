<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Item</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <style>
        #errorMessage {
            color: #f46868;
            background-color: #fbdbdb;
            padding: 10px;
            margin-top: 10px;
        }
        .container {
            margin-top: 3% !important;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <c:if test="${errorMsg != null}">
                    <div id="errorMessage">
                        <h5>${errorMsg}</h5>
                    </div>
                </c:if>
                <form role="form" action="/addItem" method="post">
                    <h2>Add New Item</h2>
                    <div class="form-group">
                        <input type="text" name="brand" id="brand" class="form-control input-lg" placeholder="Brand" tabindex="4" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="price" id="price" class="form-control input-lg" placeholder="Price" tabindex="4" required>
                    </div>
                    <div class="form-group">
                        <select id="type_select" class="js-tags-type input-lg form-control" required>
                            <option></option>
                        </select>
                        <input type="hidden" id="type" name="type">
                    </div>
                    <div class="form-group">
                        <select class="js-tags input-lg form-control " multiple="multiple" required>
                        </select>
                        <input type="hidden" id="tags" name="tags">
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6"><a href="/dashboard"><button type="button" class="btn btn-warning btn-block btn-lg">Back</button></a></div>
                        <div class="col-xs-12 col-md-6"><input type="submit" value="Add" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>

        function matchStart (term, text) {
            console.log("term", term.term);
            console.log("text", text);
            return (("" + text).toUpperCase().indexOf(term.term) >= 0);
        }


        function formatRepo (repo) {
            if (repo.loading) return repo.text;

            var markup = "<div>" + repo.tagTypeName + "</div>";

            return markup;
        }

        function formatRepoSelection (repo) {
            return repo.tagTypeName || repo.text;
        }

        $(document).ready(function () {
            $(".js-tags-type").select2({
                ajax: {
//                    url: "http://localhost:8080/tagType",
                    url: "http://139.59.101.18:8080/tagType",
//            url: "https://api.github.com/search/repositories",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.tagType,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo, // omitted for brevity, see the source of this page
                templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
                placeholder: "Select type",
                matcher: matchStart
            });

            $("#type_select").on("change", function (e) {
                e.preventDefault();
                type_val = $("#type_select").select2('data')[0].tagTypeName;
                console.log("enter")
                if (type_val == null) {
                    type_val = ""
                    $("#type").val("");
                    console.log("null")
                } else {
                    console.log(type_val)
                    console.log(type_val.toString());
                    $("#type").val(type_val.toString());
                }
            });

            function formatSelected (tag) {
                return tag.val || tag.text;
            }

            function formatResult (tag) {
                if (tag.loading) return tag.text;

                var markup = "<div>" + tag.tagTypeName + "</div>";

                return markup;
            }


            $(".js-tags").select2({
                ajax: {
//                    url: "http://localhost:8080/possibleValues",
                    url: "http://139.59.101.18:8080/possibleValues",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            type: $("#type_select").select2('data')[0].tagTypeName,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        console.log("process result");
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.PossibleValues, function (obj) {
                                obj.text = obj.text || obj.val; // replace name with the property used for the text

                                return obj;
                            }),
//                        results: data.PossibleValues,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
//            templateSelection: formatSelected,
//            templateResult: formatResult,
                tags: true,
                tokenSeparators: [",", " "],
                placeholder: "Possible values",
                matcher: matchStart
            });


            var $selected = $(".js-tags");
            var tags_array = [];

            $selected.on("change", function (e) {
                e.preventDefault();
                tags_array = $(".js-tags").select2("data").map(function(obj) {
                    console.log(obj.text);
                    return obj.text;

                });
//            console.log($(".js-tags").select2("data")[0].text);

                if (tags_array == null) {
                    tags_array = [];
                    $("#tags").val("");
                } else {
                    console.log(tags_array);
                    console.log(tags_array.toString());
                    $("#tags").val(tags_array.toString());
                }
            });
        });

    </script>
</body>

</html>
