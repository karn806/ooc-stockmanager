<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add Tag Type</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <style>
        .js-tags {
            /*width: 550px;*/
            /*height: 70px;*/
        }

        .col-xs-12 {
            /*width: 35% !important;*/
        }
        #errorMessage {
            color: #f46868;
            background-color: #fbdbdb;
            padding: 10px;
            margin-top: 10px;
        }
        .container {
            margin-top: 3% !important;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <c:if test="${errorMsg != null}">
                    <div id="errorMessage">
                        <h5>${errorMsg}</h5>
                    </div>
                </c:if>
                <form role="form" action="/addTagType" method="post">
                    <h2>Add Tag Type</h2>
                    <div class="form-group">
                        <input type="text" name="type_name" id="type_name" class="form-control input-lg" placeholder="Type Name" required>
                    </div>
                    <div class="form-group">
                        <select class="js-tags input-lg form-control" multiple="multiple" required>
                        </select>
                        <input type="hidden" id="tags" name="possibleValues">
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6"><a href="/manageTagType"><button type="button" class="btn btn-warning btn-block btn-lg">Back</button></a></div>
                        <div class="col-xs-12 col-md-6"><input type="submit" value="Add" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        var $selected = $(".js-tags");
        var tags_array = []
        $(".js-tags").select2({
            placeholder: "Possible values",
            tags: true,
            tokenSeparators: [",", " "]
        });

        $selected.on("change", function (e) {
            e.preventDefault();
            tags_array = $(".js-tags").val();
            if (tags_array == null) {
                tags_array = []
                $("#tags").val("");
            } else {
                console.log(tags_array)
                console.log(tags_array.toString());
                $("#tags").val(tags_array.toString());
            }
        })
    </script>
</body>

</html>
