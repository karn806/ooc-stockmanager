<%--
  Created by IntelliJ IDEA.
  User: ice48623
  Date: 3/1/2017 AD
  Time: 23:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add User</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .container {
            width: 40%;
        }
    </style>
</head>
<body>
<br>
<div class="container">
    <form class="form-group" action="/addUser" method="post">
        <label>Role:</label>
        <select class="js-example-basic-single form-control" name="role">
            <option value="cashier">Cashier</option>
            <option value="staff">Staff</option>
            <option value="admin">Admin</option>
        </select>
        <br>
        <label>Username:</label>
        <input class="form-control" type="text" name="username" placeholder="Enter username" required>
        <br>
        <label>Password:</label>
        <input class="form-control" type="password" name="password" placeholder="Enter password" required>
        <br>
        <label>First Name:</label>
        <input class="form-control" type="text" name="firstName" placeholder="Enter first name" required>
        <br>
        <label>Last Name:</label>
        <input class="form-control" type="text" name="lastName" placeholder="Enter last name">
        <br>
        <label>Email address:</label>
        <input class="form-control" type="text" name="email" placeholder="Enter email">
        <br>
        <label>Phone number:</label>
        <input class="form-control" type="text" name="phone" placeholder="Enter phone no.">
        <br>
        <label>Address:</label>
        <input class="form-control" type="text" name="address" placeholder="Enter address">
        <br>
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="/userList" class="btn btn-primary">Back</a>
    </form>
    <br>

</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".js-example-basic-single").select2();
    });
</script>
</body>
</html>
