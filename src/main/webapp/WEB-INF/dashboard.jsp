<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.bootstrap.min.css" />
    <style>
        .table-css {
            padding: 10px;
        }
    </style>
</head>
<body>
    <center><h2>Dashboard</h2></center>
    <div class="container" style="margin-top: 5%;">
        <div class="btn-group" role="group" style="margin-bottom: 10px;">
            <a href="/logout"><button type="button" class="btn btn-danger">Logout</button></a>
            <c:if test="${role == 'ROLE_ADMIN' || role == 'ROLE_STAFF'}">
                <a href="/addItem"><button type="button" class="btn btn-info">Add Item</button></a>
                <a href="/manageTagType"><button type="button" class="btn btn-info">Tag Type</button></a>
            </c:if>
            <c:if test="${role == 'ROLE_ADMIN'}">
                <a href="/userList"><button type="button" class="btn btn-info">Users</button></a>
            </c:if>
        </div>

        <div class="panel panel-default table-css">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>brand</th>
                        <th>price</th>
                        <th>quantity</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="item" items="${items}">
                        <c:choose>
                            <c:when test="${item.status == 'active'}">
                                <tr>
                                    <td>${item.name}</td>
                                    <td>${item.brand}</td>
                                    <td>${item.price}</td>
                                    <td>${item.quantity}</td>
                                </tr>
                            </c:when>
                        <c:otherwise>
                        </c:otherwise>
                        </c:choose>

                   
                    </c:forEach>
                </tbody>
            </table>

        </div>
        <div style="margin-top: 5px;">
            <form action="/updateQuantity" method="get" style="display: inline-block">
                <input class="selectedItem" type="hidden" name="selectedItem">
                <input id="update_button" type="submit" class="btn btn-info" value="Update Stock" style="display: none">
            </form>
            <form action="/editItem" method="get" id="editItem" style="display: inline-block">
                <input class="selectedItem" type="hidden" name="selectedItem">
                <input id="edit_button" type="submit" class="btn btn-success" value="Edit" style="display: none">
            </form>
            <form action="/deleteItem" method="post" id="deleteItem" style="display: inline-block">
                <input class="selectedItem" type="hidden" name="selectedItem">
                <input id="del_button" type="submit" class="btn btn-danger" value="Delete" style="display: none">
            </form>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#example').DataTable({
                select: {
                    style: 'single'
                }
            });

            table.on( 'click', 'tr', function () {
                var selected = table.row( this ).data();
                $('.selectedItem').val(selected[0].toString());
            } );

            table.on( 'deselect', function () {
                $('#del_button').css('display', 'none');
                $('#update_button').css('display', 'none');
                $('#edit_button').css('display', 'none');
            } );

            table.on( 'select', function() {
                $('#del_button').css('display', '');
                $('#update_button').css('display', '');
                $('#edit_button').css('display', '');
            } )

            $('#del_button').click(function(e) {
                e.preventDefault();
                var msg = 'Are you sure?';
                bootbox.confirm(msg, function(result) {
                    if (result) {
                       $('#deleteItem').submit();
                    }
                });
            });
        } );

    </script>
</body>
</html>
