<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Tag Type</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <form role="form" action="/editTagType" method="post">
                    <h2>Edit Tag Type</h2>
                    <div class="form-group">
                        <input type="text" name="type_name" id="type_name" class="form-control input-lg" value="${typeName}" placeholder="Type Name" required readonly>
                    </div>
                    <div class="form-group">
                        <select class="js-tags input-lg form-control" multiple="multiple" required>
                            <c:forEach var="val" items="${posVals}">
                                <option selected="selected">${val}</option>
                            </c:forEach>
                        </select>
                        <input type="hidden" id="tags" name="possibleValues">
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6"><a href="/dashboard"><button type="button" class="btn btn-warning btn-block btn-lg">Back</button></a></div>
                        <div class="col-xs-12 col-md-6"><input type="submit" value="Save" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(".js-tags").select2({
            placeholder: "Possible values",
            tags: true,
            tokenSeparators: [",", " "]
        });

        $(".js-tags").on("change", function (e) {
            e.preventDefault();
            tags_array = $(".js-tags").select2("data").map(function(obj) {
                console.log(obj.text);
                return obj.text;

            });
//            console.log($(".js-tags").select2("data")[0].text);

            if (tags_array == null) {
                tags_array = [];
                $("#tags").val("");
            } else {
                console.log(tags_array);
                console.log(tags_array.toString());
                $("#tags").val(tags_array.toString());
            }
        });
    </script>
</body>
</html>
