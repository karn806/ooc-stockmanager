<%--
  Created by IntelliJ IDEA.
  User: ice48623
  Date: 3/1/2017 AD
  Time: 23:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit User</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .container {
            width: 40%;
        }
        #red-txt {
            color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <br>
    <form class="form-group" action="/editUser" method="post">
        <div id="red-txt">* only fill in the field you want to edit. </div><br>
        <label>Role:</label>
        <%--<select class="js-example-basic-single form-control" name="role">--%>
            <%--<option value="cashier">Cashier</option>--%>
            <%--<option value="staff">Staff</option>--%>
            <%--<option value="admin">Admin</option>--%>
        <%--</select>--%>
        <label>Username:</label>
        <input class="form-control" type="text" placeholder="${username}" disabled>
        <br>
        <label>New Password:</label>
        <input class="form-control" type="password" name="password" placeholder="Enter password">
        <br>
        <label>New First Name:</label>
        <input class="form-control" type="text" name="firstName" placeholder="Enter first name">
        <br>
        <label>New Last Name:</label>
        <input class="form-control" type="text" name="lastName" placeholder="Enter last name">
        <br>
        <label>New Email address:</label>
        <input class="form-control" type="text" name="email" placeholder="Enter email">
        <br>
        <label>New Phone number:</label>
        <input class="form-control" type="text" name="phone" placeholder="Enter phone no.">
        <br>
        <label>New Address:</label>
        <input class="form-control" type="text" name="address" placeholder="Enter address">
        <br>
        <input type="hidden" name="username" value="${username}">
        <input class="btn btn-success" type="submit" value="Confirm">
        <a href="/userList" class="btn btn-primary">Cancel</a>
    </form>
<br>

</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".js-example-basic-single").select2();
    });
</script>
</body>
</html>
