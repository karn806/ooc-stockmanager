<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ice48623
  Date: 3/1/2017 AD
  Time: 17:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>


<div class="container" style="margin-top: 5%;">
    <div class="btn-group" role="group" style="margin-bottom: 10px;">
        <a href="/logout"><button type="button" class="btn btn-danger">Logout</button></a>
        <a href="/addItem"><button type="button" class="btn btn-primary">Add Item</button></a>
        <a href="/addItemType"><button type="button" class="btn btn-warning">Add Item Type</button></a>
        <a href="/report"><button type="button" class="btn btn-warning">Report</button></a>
        <a href="/userList"><button type="button" class="btn btn-info">Users</button></a>
        <a href="/dashboard"><button type="button" class="btn btn-success">Show all users</button></a>
    </div>
    <div>
        <form class="form-group" action="/searchItem" method="post">
            <input type="text" style="width: 20%; margin-bottom: 5px;" class="form-control" name="searchInput" placeholder="Search by name...">
            <input type="submit" class="btn btn-dafault" value="Search">
        </form>
    </div>
    <%--<a href="/addUser">Add User</a>--%>
    <%--<a href="/editUser">Edit User</a>--%>



    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>
        <table class="table">
            <tr>
                <th>name</th>
                <th>brand</th>
                <th>price</th>
            </tr>
            <c:forEach var="item" items="${items}">
                <tr>
                    <td>${item.name}</td>
                    <td>${item.brand}</td>
                    <td>${item.price}</td>
                </tr>
            </c:forEach>
        </table>

    </div>
</div>
</body>
</html>
