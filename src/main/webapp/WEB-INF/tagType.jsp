<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manage Tag Type</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.bootstrap.min.css" />
    <style>
        .wrapper {
            margin-top: 5%;
        }
        .table-css {
            padding: 10px;
        }
    </style>
</head>
<body>
    <div class="container wrapper">
        <h2>Tag Type</h2>
        <div class="panel panel-default table-css">
            <table id="tagTypeTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>All Possible Tags</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach var="tagType" items="${tagTypes}">
                    <c:set var="tags" value="${fn:replace(tagType.possibleValues, '[', '')}"/>
                    <c:set var="tags" value="${fn:replace(tags, ']', '')}"/>
                    <tr>
                        <td>${tagType.typeName}</td>
                        <td>
                            ${tags}
                        </td>
                    </tr>


                </c:forEach>
                </tbody>
            </table>

        </div>

        <div>
            <form action="/dashboard" method="get" style="display: inline-block">
                <input id="back" type="submit" class="btn btn-warning" value="Back">
            </form>
            <form action="/addTagType" method="get" style="display: inline-block">
                <input type="submit" class="btn btn-info" value="Add Tag Type">
            </form>
            <form action="/editTagType" method="get" style="display: inline-block">
                <input id="selectedTagType" type="hidden" name="selectedTagType">
                <input id="edit_button" type="submit" class="btn btn-success" value="Edit" style="display: none">
            </form>

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#tagTypeTable').DataTable({
                select: {
                    style: 'single'
                }
            });

            table.on( 'click', 'tr', function () {
                var selected = table.row( this ).data();
                $('#selectedTagType').val(selected[0].toString());
            } );

            table.on( 'select', function() {
                $('#edit_button').css('display', '');
            } )

            table.on( 'deselect', function () {
                $('#edit_button').css('display', 'none');
            } );

        } );

    </script>
</body>
</html>
