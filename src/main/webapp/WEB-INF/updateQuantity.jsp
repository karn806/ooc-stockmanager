<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Update Quantity</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <form role="form" action="/updateQuantity" method="post">
                    <h2>Update Item Quantity</h2>
                    <div class="form-group">
                        <input type="text" name="itemName" id="itemName" value="${itemName}" class="form-control input-lg" placeholder="Name" tabindex="4" required readonly>
                    </div>
                    <div class="form-group">
                        <input type="text" name="price" id="price" value="${price}" class="form-control input-lg" placeholder="Price" tabindex="4" required readonly>
                    </div>
                    <div class="form-group">
                        <input type="button" id="minute" value="-">
                        <input type="text" id="quantity" name="quantity" value="${quantity}">
                        <input type="button" id="plus" value="+">
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6"><a href="/dashboard"><button type="button" class="btn btn-warning btn-block btn-lg">Back</button></a></div>
                        <div class="col-xs-12 col-md-6"><input type="submit" value="Save" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            $('#plus').on('click', function(){
                var previousQuantity = parseInt($('#quantity').val());
                var newQuantity = previousQuantity + 1;
                $('#quantity').val(newQuantity);
            })

            $('#minute').on('click', function(){
                var previousQuantity = parseInt($('#quantity').val());
                var newQuantity = previousQuantity - 1;
                $('#quantity').val(newQuantity);
            })
        </script>
    </body>
</html>
