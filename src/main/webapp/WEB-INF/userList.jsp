<%--
  Created by IntelliJ IDEA.
  User: karn806
  Date: 3/2/17
  Time: 9:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Users</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.bootstrap.min.css" />
    <style>
        .table-css {
            padding: 10px;
        }
    </style>
</head>
<body>
<br><br>
    <div class="container" style="margin-top: 2%">
        <center><h2>Users</h2></center>
        <div class="panel panel-default table-css">
            <table class="table table-striped table-bordered" id="example">
                <thead>
                <tr>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone no.</th>
                    <th>Address</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="user" items="${users}">
                    <tr>
                        <td>${user.username}</td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.email}</td>
                        <td>${user.phone}</td>
                        <td>${user.address}</td>
                        <form action="/userList" method="post">
                            <td>
                                <input type="hidden" name="action" value="edit"/>
                                <input type="hidden" name="username" value="${user.username}"/>
                                <input class="btn btn-success" type="submit" value="Edit"/>
                            </td>
                        </form>
                        <form action="/userList" method="post">
                            <td>
                                <input type="hidden" name="action" value="delete"/>
                                <input type="hidden" name="username" value="${user.username}"/>
                                <input class="btn btn-danger" type="submit" value="Delete"/>
                            </td>
                        </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="form-group form-inline" role="group" >
            <a href="/dashboard"><button type="button" class="btn btn-warning">Back</button></a>
            <a href="/addUser"><button type="button" class="btn btn-info">Add User</button></a>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#example').DataTable({
                select: true
            });

//            $('#example tbody').on( 'click', 'tr', function () {
//                select: true
//                var selected = table.row( this ).data()
//                $('itemName').val(selected[0].toString());
//            } );
        } );
    </script>
</body>
</html>
